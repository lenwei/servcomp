# SERVCOMP

## Setup

Before running the application locally, a Postgres installation is required.  
See: https://www.postgresql.org/download/
Postgres database usually runs on port 5432 by default and this is also the case for this application.  

To run the application, npm and Angular are needed.
See https://www.npmjs.com/ and https://angular.io/guide/setup-local

Download or clone the repository on your local machine.

To start the backend, use following command in the backend folder 
`mvnw clean spring-boot:run`

To start the frontend, use following command in the frontend folder  
`npm install` initially to install local dependencies to the node_modules folder.  
`ng serve` to start the application.