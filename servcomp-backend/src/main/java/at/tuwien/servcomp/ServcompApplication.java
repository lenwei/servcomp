package at.tuwien.servcomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServcompApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServcompApplication.class, args);
	}

}