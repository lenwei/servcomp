package at.tuwien.servcomp.endpoint;

import at.tuwien.servcomp.endpoint.dto.ProviderDTO;
import at.tuwien.servcomp.endpoint.mapper.ProviderMapper;
import at.tuwien.servcomp.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/provider")
public class ProviderEndpoint {

    private final ProviderService providerService;
    private final ProviderMapper providerMapper;

    @Autowired
    public ProviderEndpoint(ProviderService providerService, ProviderMapper providerMapper) {
        this.providerService = providerService;
        this.providerMapper = providerMapper;
    }

    @GetMapping(path = "/{id}")
    public ProviderDTO getOneById(@PathVariable Long id){
        return providerMapper.toProviderDTO(providerService.getOneById(id));
    }

    @PostMapping
    public void saveProvider(@RequestBody ProviderDTO provider){
        providerService.saveProvider(providerMapper.toProvider(provider));
    }
}
