package at.tuwien.servcomp.endpoint;

import at.tuwien.servcomp.endpoint.dto.QuestionDTO;
import at.tuwien.servcomp.endpoint.mapper.QuestionMapper;
import at.tuwien.servcomp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/question")
public class QuestionEndpoint {

    private QuestionService questionService;
    private QuestionMapper questionMapper;

    @Autowired
    public QuestionEndpoint(QuestionService questionService, QuestionMapper questionMapper) {
        this.questionService = questionService;
        this.questionMapper = questionMapper;
    }

    @GetMapping(path = "/{id}")
    public QuestionDTO getOneById(@PathVariable Long id){
        return questionMapper.toQuestionDTO(questionService.getOneById(id));
    }

    @PostMapping
    public void saveQuestion(@RequestBody QuestionDTO questionDTO){
        questionService.saveQuestion(questionMapper.toQuestion(questionDTO));
    }
}
