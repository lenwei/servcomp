package at.tuwien.servcomp.endpoint;

import at.tuwien.servcomp.endpoint.dto.RatingDTO;
import at.tuwien.servcomp.endpoint.mapper.RatingMapper;
import at.tuwien.servcomp.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/rating")
public class RatingEndpoint {

    private RatingService ratingService;
    private RatingMapper ratingMapper;

    @Autowired
    public RatingEndpoint(RatingService ratingService, RatingMapper ratingMapper) {
        this.ratingService = ratingService;
        this.ratingMapper = ratingMapper;
    }

    @GetMapping(path = "/{id}")
    public RatingDTO getOneById(@PathVariable Long id){
        return ratingMapper.toRatingDTO(ratingService.getOneById(id));
    }

    @GetMapping(path = "/by-question/{questionId}")
    public List<RatingDTO> getAllToQuestionId(@PathVariable Long questionId){
        return ratingMapper.toRatingDTOList(ratingService.getAllToQuestionId(questionId));
    }

    @PostMapping
    public void saveRating(@RequestBody RatingDTO ratingDTO){
        ratingService.saveRating(ratingMapper.toRating(ratingDTO));
    }

    @PostMapping(path = "/ratings")
    public void saveRatings(@RequestBody List<RatingDTO> ratings){
        ratingService.saveRatings(ratingMapper.toRatingList(ratings));
    }
}
