package at.tuwien.servcomp.endpoint;

import at.tuwien.servcomp.endpoint.dto.QuestionDTO;
import at.tuwien.servcomp.endpoint.dto.ServiceDTO;
import at.tuwien.servcomp.endpoint.mapper.QuestionMapper;
import at.tuwien.servcomp.endpoint.mapper.ServiceMapper;
import at.tuwien.servcomp.service.ServiceService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/service")
@Api(value = "/service")
public class ServiceEndpoint {

    private ServiceService serviceService;
    private ServiceMapper serviceMapper;
    private QuestionMapper questionMapper;

    @Autowired
    public ServiceEndpoint(ServiceService serviceService, ServiceMapper serviceMapper, QuestionMapper questionMapper) {
        this.serviceService = serviceService;
        this.serviceMapper = serviceMapper;
        this.questionMapper = questionMapper;
    }

    @GetMapping(path = "/{id}")
    public ServiceDTO getOneById(@PathVariable Long id){
        return serviceMapper.toServiceDTO(serviceService.getOneById(id));
    }

    @GetMapping(path = "/services")
    public List<ServiceDTO> getAllServices(){
        return serviceMapper.toServiceDTOList(serviceService.getAll());
    }

    @PostMapping
    public void saveService(@RequestBody ServiceDTO serviceDTO){
        serviceService.saveService(serviceMapper.toService(serviceDTO));
    }

    @GetMapping(path = "/by-service/{serviceId}")
    public List<QuestionDTO> getQuestionsByServiceId(@PathVariable Long serviceId){
        return questionMapper.toQuestionDTOList(serviceService.getQuestionsToService(serviceId));
    }
}
