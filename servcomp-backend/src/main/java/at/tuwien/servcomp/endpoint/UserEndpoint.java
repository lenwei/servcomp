package at.tuwien.servcomp.endpoint;

import at.tuwien.servcomp.endpoint.dto.UserDTO;
import at.tuwien.servcomp.endpoint.mapper.UserMapper;
import at.tuwien.servcomp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/user")
public class UserEndpoint {

    private UserService userService;
    private UserMapper userMapper;

    @Autowired
    public UserEndpoint(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(path = "/{id}")
    public UserDTO getOneById(@PathVariable Long id){
        return userMapper.toUserDTO(userService.getOneById(id));
    }

    @GetMapping(path = "/by-name/{username}")
    public UserDTO getOneByName(@PathVariable String username){
        return userMapper.toUserDTO(userService.getOneByName(username));
    }

    @PostMapping
    public void saveUser(@RequestBody UserDTO userDTO){
        userService.saveUser(userMapper.toUser(userDTO));
    }
}
