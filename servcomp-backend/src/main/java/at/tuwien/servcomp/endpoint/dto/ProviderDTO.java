package at.tuwien.servcomp.endpoint.dto;

import java.util.Set;

public class ProviderDTO {

    private Long id;
    private Set<ServiceDTO> services;
    private String name;

    public ProviderDTO(Long id, Set<ServiceDTO> services, String name) {
        this.id = id;
        this.services = services;
        this.name = name;
    }

    public ProviderDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ServiceDTO> getServices() {
        return services;
    }

    public void setServices(Set<ServiceDTO> services) {
        this.services = services;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderDTO that = (ProviderDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (services != null ? !services.equals(that.services) : that.services != null) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (services != null ? services.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProviderDTO{" +
                "id=" + id +
                ", services=" + services +
                ", name='" + name + '\'' +
                '}';
    }
}
