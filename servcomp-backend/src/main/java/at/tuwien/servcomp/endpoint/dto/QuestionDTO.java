package at.tuwien.servcomp.endpoint.dto;

import java.util.Set;

public class QuestionDTO {

    private Long id;
    private String dimension;
    private String perception;
    private String expectation;

    public QuestionDTO(Long id, Set<RatingDTO> ratings, String dimension, String perception, String expectation) {
        this.id = id;
        this.dimension = dimension;
        this.perception = perception;
        this.expectation = expectation;
    }

    public QuestionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public String getExpectation() {
        return expectation;
    }

    public void setExpectation(String expectation) {
        this.expectation = expectation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionDTO that = (QuestionDTO) o;

        if (!perception.equals(that.perception)) return false;
        if (!expectation.equals(that.expectation)) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return dimension != null ? dimension.equals(that.dimension) : that.dimension == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dimension != null ? dimension.hashCode() : 0);
        result = 31 * result + (perception != null ? perception.hashCode() : 0);
        result = 31 * result + (expectation != null ? expectation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
                "id=" + id +
                ", dimension='" + dimension + '\'' +
                ", perception='" + perception + '\'' +
                ", expectation='" + expectation + '\'' +
                '}';
    }
}
