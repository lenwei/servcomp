package at.tuwien.servcomp.endpoint.dto;

public class RatingDTO {

    private Long id;
    private QuestionDTO question;
    private UserDTO user;
    private float score;

    public RatingDTO(Long id, QuestionDTO question, UserDTO user, float score) {
        this.id = id;
        this.question = question;
        this.user = user;
        this.score = score;
    }

    public RatingDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionDTO question) {
        this.question = question;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RatingDTO ratingDTO = (RatingDTO) o;

        if (Float.compare(ratingDTO.score, score) != 0) return false;
        if (id != null ? !id.equals(ratingDTO.id) : ratingDTO.id != null) return false;
        if (question != null ? !question.equals(ratingDTO.question) : ratingDTO.question != null) return false;
        return user != null ? user.equals(ratingDTO.user) : ratingDTO.user == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (question != null ? question.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (score != +0.0f ? Float.floatToIntBits(score) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RatingDTO{" +
                "id=" + id +
                ", question=" + question +
                ", user=" + user +
                ", score=" + score +
                '}';
    }
}
