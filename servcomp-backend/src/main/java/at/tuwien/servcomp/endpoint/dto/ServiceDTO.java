package at.tuwien.servcomp.endpoint.dto;

import java.util.List;
import java.util.Set;

public class ServiceDTO {

    private Long id;
    private String name;
    private ProviderDTO provider;
    private List<QuestionDTO> questions;
    private float overallScore;
    private float reliabilityScore;
    private float assuranceScore;
    private float tangiblesScore;
    private float empathyScore;
    private float responsivenessScore;

    public ServiceDTO(Long id, String name, ProviderDTO provider, List<QuestionDTO> questions, float overallScore,
                      float reliabilityScore, float assuranceScore, float tangiblesScore,
                      float empathyScore, float responsivenessScore) {
        this.id = id;
        this.name = name;
        this.provider = provider;
        this.questions = questions;
        this.overallScore = overallScore;
        this.reliabilityScore = reliabilityScore;
        this.assuranceScore = assuranceScore;
        this.tangiblesScore = tangiblesScore;
        this.empathyScore = empathyScore;
        this.responsivenessScore = responsivenessScore;
    }

    public ServiceDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<QuestionDTO> getQuestions() {
       return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public ProviderDTO getProvider() {
        return provider;
    }

    public void setProvider(ProviderDTO provider) {
        this.provider = provider;
    }

    public float getOverallScore() {
        return overallScore;
    }

    public void setOverallScore(float overallScore) {
        this.overallScore = overallScore;
    }

    public float getReliabilityScore() {
        return reliabilityScore;
    }

    public void setReliabilityScore(float reliabilityScore) {
        this.reliabilityScore = reliabilityScore;
    }

    public float getAssuranceScore() {
        return assuranceScore;
    }

    public void setAssuranceScore(float assuranceScore) {
        this.assuranceScore = assuranceScore;
    }

    public float getTangiblesScore() {
        return tangiblesScore;
    }

    public void setTangiblesScore(float tangiblesScore) {
        this.tangiblesScore = tangiblesScore;
    }

    public float getEmpathyScore() {
        return empathyScore;
    }

    public void setEmpathyScore(float empathyScore) {
        this.empathyScore = empathyScore;
    }

    public float getResponsivenessScore() {
        return responsivenessScore;
    }

    public void setResponsivenessScore(float responsivenessScore) {
        this.responsivenessScore = responsivenessScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceDTO that = (ServiceDTO) o;

        if (Float.compare(that.overallScore, overallScore) != 0) return false;
        if (Float.compare(that.reliabilityScore, reliabilityScore) != 0) return false;
        if (Float.compare(that.assuranceScore, assuranceScore) != 0) return false;
        if (Float.compare(that.tangiblesScore, tangiblesScore) != 0) return false;
        if (Float.compare(that.empathyScore, empathyScore) != 0) return false;
        if (Float.compare(that.responsivenessScore, responsivenessScore) != 0) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return (provider != null ? !provider.equals(that.provider) : that.provider != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        result = 31 * result + (overallScore != +0.0f ? Float.floatToIntBits(overallScore) : 0);
        result = 31 * result + (reliabilityScore != +0.0f ? Float.floatToIntBits(reliabilityScore) : 0);
        result = 31 * result + (assuranceScore != +0.0f ? Float.floatToIntBits(assuranceScore) : 0);
        result = 31 * result + (tangiblesScore != +0.0f ? Float.floatToIntBits(tangiblesScore) : 0);
        result = 31 * result + (empathyScore != +0.0f ? Float.floatToIntBits(empathyScore) : 0);
        result = 31 * result + (responsivenessScore != +0.0f ? Float.floatToIntBits(responsivenessScore) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ServiceDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", provider=" + provider +
                ", questions=" + questions +
                ", overallScore=" + overallScore +
                ", reliabilityScore=" + reliabilityScore +
                ", assuranceScore=" + assuranceScore +
                ", tangiblesScore=" + tangiblesScore +
                ", empathyScore=" + empathyScore +
                ", responsivenessScore=" + responsivenessScore +
                '}';
    }
}
