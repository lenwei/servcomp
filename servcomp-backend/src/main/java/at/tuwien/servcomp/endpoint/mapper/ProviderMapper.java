package at.tuwien.servcomp.endpoint.mapper;

import at.tuwien.servcomp.endpoint.dto.ProviderDTO;
import at.tuwien.servcomp.model.Provider;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses={ServiceMapper.class})
public interface ProviderMapper {

    @Mapping(target = "services", ignore = true)
    Provider toProvider(ProviderDTO providerDTO);

    @Mapping(target = "services", ignore = true)
    ProviderDTO toProviderDTO(Provider provider);

    List<Provider> toProviderList(List<ProviderDTO> providerList);

    List<ProviderDTO> toProviderDTOList(List<Provider> providerList);
}
