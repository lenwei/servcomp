package at.tuwien.servcomp.endpoint.mapper;

import at.tuwien.servcomp.endpoint.dto.QuestionDTO;
import at.tuwien.servcomp.model.Question;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses={RatingMapper.class, ServiceMapper.class})
public interface QuestionMapper {

    Question toQuestion(QuestionDTO questionDTO);

    QuestionDTO toQuestionDTO(Question question);

    List<QuestionDTO> toQuestionDTOList(List<Question> questions);
}
