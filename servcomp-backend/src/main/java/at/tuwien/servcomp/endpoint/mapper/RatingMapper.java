package at.tuwien.servcomp.endpoint.mapper;

import at.tuwien.servcomp.endpoint.dto.RatingDTO;
import at.tuwien.servcomp.model.Rating;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses={QuestionMapper.class, UserMapper.class})
public interface RatingMapper {

    Rating toRating(RatingDTO ratingDTO);

    RatingDTO toRatingDTO(Rating rating);

    List<RatingDTO> toRatingDTOList(List<Rating> ratings);

    List<Rating> toRatingList(List<RatingDTO> ratings);
}
