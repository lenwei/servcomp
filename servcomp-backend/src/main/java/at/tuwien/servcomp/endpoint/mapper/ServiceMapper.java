package at.tuwien.servcomp.endpoint.mapper;

import at.tuwien.servcomp.endpoint.dto.ServiceDTO;
import at.tuwien.servcomp.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses={ProviderMapper.class, QuestionMapper.class})
public interface ServiceMapper {

    Service toService(ServiceDTO serviceDTO);

    ServiceDTO toServiceDTO(Service service);

    List<ServiceDTO> toServiceDTOList(List<Service> serviceList);

    List<Service> toServiceList(List<ServiceDTO> serviceList);
}
