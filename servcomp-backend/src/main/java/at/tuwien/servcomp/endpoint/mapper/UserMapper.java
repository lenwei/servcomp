package at.tuwien.servcomp.endpoint.mapper;

import at.tuwien.servcomp.endpoint.dto.UserDTO;
import at.tuwien.servcomp.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses={RatingMapper.class})
public interface UserMapper {

    User toUser(UserDTO userDTO);

    UserDTO toUserDTO(User user);

    List<User> toUserList(List<UserDTO> userList);

    List<UserDTO> toUserDTOList(List<User> userList);
}
