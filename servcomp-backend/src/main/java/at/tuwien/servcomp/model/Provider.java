package at.tuwien.servcomp.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "provider")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "provider")
    private Set<Service> services;

    @Column(name = "name")
    private String name;

    public Provider(Long id, Set<Service> services, String name) {
        this.id = id;
        this.services = services;
        this.name = name;
    }

    public Provider() {
    }

    //*****************************************//
    //      GETTER AND SETTER METHODS          //
    //*****************************************//

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Provider provider = (Provider) o;

        if (id != null ? !id.equals(provider.id) : provider.id != null) return false;
        if (services != null ? !services.equals(provider.services) : provider.services != null) return false;
        return name != null ? name.equals(provider.name) : provider.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;

        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "id=" + id +
                ", services=" + services +
                ", name='" + name + '\'' +
                '}';
    }
}
