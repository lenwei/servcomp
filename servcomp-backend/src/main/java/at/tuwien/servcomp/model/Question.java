package at.tuwien.servcomp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "dimension")
    private String dimension;

    @Column(name = "expectation")
    private String expectation;

    @Column(name = "perception")
    private String perception;

    public Question(Long id, String dimension, String expectation, String perception) {
        this.id = id;
        this.dimension = dimension;
        this.expectation = expectation;
        this.perception = perception;
    }

    public Question() {
    }

    //*****************************************//
    //      GETTER AND SETTER METHODS          //
    //*****************************************//


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getExpectation() {
        return expectation;
    }

    public void setExpectation(String expectation) {
        this.expectation = expectation;
    }

    public String getPerception() {
        return perception;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Question question = (Question) o;

        if (!expectation.equals(question.expectation)) return false;
        if (!perception.equals(question.perception)) return false;
        if (id != null ? !id.equals(question.id) : question.id != null) return false;
        //if (ratings != null ? !ratings.equals(question.ratings) : question.ratings != null) return false;
        return dimension != null ? dimension.equals(question.dimension) : question.dimension == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        //result = 31 * result + (ratings != null ? ratings.hashCode() : 0);
        result = 31 * result + (dimension != null ? dimension.hashCode() : 0);
        result = 31 * result + (expectation != null ? expectation.hashCode() : 0);
        result = 31 * result + (perception != null ? perception.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", dimension='" + dimension + '\'' +
                ", expectation='" + expectation + '\'' +
                ", perception='" + perception + '\'' +
                '}';
    }
}
