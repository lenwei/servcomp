package at.tuwien.servcomp.model;

import javax.persistence.*;

@Entity
@Table(name = "rating")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fk_question")
    private Question question;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "score")
    private float score;

    public Rating(Long id, Question question, User user, float score) {
        this.id = id;
        this.question = question;
        this.user = user;
        this.score = score;
    }

    public Rating() {
    }

    //*****************************************//
    //      GETTER AND SETTER METHODS          //
    //*****************************************//

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (Float.compare(rating.score, score) != 0) return false;
        if (id != null ? !id.equals(rating.id) : rating.id != null) return false;
        if (question != null ? !question.equals(rating.question) : rating.question != null) return false;
        return user != null ? user.equals(rating.user) : rating.user == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (question != null ? question.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (score != +0.0f ? Float.floatToIntBits(score) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "id=" + id +
                ", question=" + question +
                ", user=" + user +
                ", score=" + score +
                '}';
    }
}
