package at.tuwien.servcomp.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "service")
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_provider")
    private Provider provider;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Question> questions;

    @Column(name = "overall_score")
    private float overallScore;

    @Column(name = "reliability_score")
    private float reliabilityScore;

    @Column(name = "assurance_score")
    private float assuranceScore;

    @Column(name = "tangibles_score")
    private float tangiblesScore;

    @Column(name = "empathy_score")
    private float empathyScore;

    @Column(name = "responsiveness_score")
    private float responsivenessScore;

    public Service(Long id, String name, Provider provider, List<Question> questions, float overallScore,
                   float reliabilityScore, float assuranceScore, float tangiblesScore, float empathyScore,
                   float responsivenessScore) {
        this.id = id;
        this.name = name;
        this.provider = provider;
        this.questions = questions;
        this.overallScore = overallScore;
        this.reliabilityScore = reliabilityScore;
        this.assuranceScore = assuranceScore;
        this.tangiblesScore = tangiblesScore;
        this.empathyScore = empathyScore;
        this.responsivenessScore = responsivenessScore;
    }

    public Service() {
    }

    //*****************************************//
    //      GETTER AND SETTER METHODS          //
    //*****************************************//

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public float getReliabilityScore() {
        return reliabilityScore;
    }

    public void setReliabilityScore(float reliabilityScore) {
        this.reliabilityScore = reliabilityScore;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public float getOverallScore() {
        return overallScore;
    }

    public void setOverallScore(float overallScore) {
        this.overallScore = overallScore;
    }

    public float getAssuranceScore() {
        return assuranceScore;
    }

    public void setAssuranceScore(float assuranceScore) {
        this.assuranceScore = assuranceScore;
    }

    public float getTangiblesScore() {
        return tangiblesScore;
    }

    public void setTangiblesScore(float tangiblesScore) {
        this.tangiblesScore = tangiblesScore;
    }

    public float getEmpathyScore() {
        return empathyScore;
    }

    public void setEmpathyScore(float empathyScore) {
        this.empathyScore = empathyScore;
    }

    public float getResponsivenessScore() {
        return responsivenessScore;
    }

    public void setResponsivenessScore(float responsivenessScore) {
        this.responsivenessScore = responsivenessScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Service service = (Service) o;

        if (Float.compare(service.overallScore, overallScore) != 0) return false;
        if (Float.compare(service.reliabilityScore, reliabilityScore) != 0) return false;
        if (Float.compare(service.assuranceScore, assuranceScore) != 0) return false;
        if (Float.compare(service.tangiblesScore, tangiblesScore) != 0) return false;
        if (Float.compare(service.empathyScore, empathyScore) != 0) return false;
        if (Float.compare(service.responsivenessScore, responsivenessScore) != 0) return false;
        if (id != null ? !id.equals(service.id) : service.id != null) return false;
        return (provider != null ? !provider.equals(service.provider) : service.provider != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (questions != null ? questions.hashCode() : 0);
        result = 31 * result + (overallScore != +0.0f ? Float.floatToIntBits(overallScore) : 0);
        result = 31 * result + (reliabilityScore != +0.0f ? Float.floatToIntBits(reliabilityScore) : 0);
        result = 31 * result + (assuranceScore != +0.0f ? Float.floatToIntBits(assuranceScore) : 0);
        result = 31 * result + (tangiblesScore != +0.0f ? Float.floatToIntBits(tangiblesScore) : 0);
        result = 31 * result + (empathyScore != +0.0f ? Float.floatToIntBits(empathyScore) : 0);
        result = 31 * result + (responsivenessScore != +0.0f ? Float.floatToIntBits(responsivenessScore) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", provider=" + provider +
                ", questions=" + questions +
                ", overallScore=" + overallScore +
                ", reliabilityScore=" + reliabilityScore +
                ", assuranceScore=" + assuranceScore +
                ", tangiblesScore=" + tangiblesScore +
                ", empathyScore=" + empathyScore +
                ", responsivenessScore=" + responsivenessScore +
                '}';
    }
}
