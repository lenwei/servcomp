package at.tuwien.servcomp.persistence;

import at.tuwien.servcomp.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

    List<Rating> findAllByQuestionId(Long questionId);

    Long countByQuestion_DimensionAndQuestion_Id(String dimension, Long id);
}
