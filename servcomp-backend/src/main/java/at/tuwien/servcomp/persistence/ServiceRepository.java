package at.tuwien.servcomp.persistence;

import at.tuwien.servcomp.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

    @Query(
            value = "SELECT service_id FROM service_questions s WHERE s.questions_id = ?1",
            nativeQuery = true)
    Long findByQuestionId(Long questionId);
}