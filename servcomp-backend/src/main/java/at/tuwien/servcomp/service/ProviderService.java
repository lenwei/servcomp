package at.tuwien.servcomp.service;

import at.tuwien.servcomp.exception.NotFoundException;
import at.tuwien.servcomp.model.Provider;
import at.tuwien.servcomp.persistence.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {

    private ProviderRepository providerRepository;

    @Autowired
    public ProviderService(ProviderRepository providerRepository){
        this.providerRepository = providerRepository;
    }

    public Provider getOneById(Long id){
        return providerRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public void saveProvider(Provider provider){
        providerRepository.save(provider);
    }
}
