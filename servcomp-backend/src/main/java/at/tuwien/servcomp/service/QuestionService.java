package at.tuwien.servcomp.service;

import at.tuwien.servcomp.exception.NotFoundException;
import at.tuwien.servcomp.model.Question;
import at.tuwien.servcomp.persistence.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository){
        this.questionRepository = questionRepository;
    }

    public Question getOneById(Long id){
        return questionRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public void saveQuestion(Question question){
        questionRepository.save(question);
    }
}
