package at.tuwien.servcomp.service;

import at.tuwien.servcomp.exception.NotFoundException;
import at.tuwien.servcomp.model.Rating;
import at.tuwien.servcomp.model.User;
import at.tuwien.servcomp.persistence.RatingRepository;
import at.tuwien.servcomp.persistence.ServiceRepository;
import at.tuwien.servcomp.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RatingService {

    private RatingRepository ratingRepository;
    private UserRepository userRepository;
    private ServiceRepository serviceRepository;

    @Autowired
    public RatingService(RatingRepository ratingRepository, UserRepository userRepository, ServiceRepository serviceRepository){
        this.ratingRepository = ratingRepository;
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
    }

    public Rating getOneById(Long id){
        return ratingRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<Rating> getAllToQuestionId(Long questionId){
        return ratingRepository.findAllByQuestionId(questionId);
    }

    public void saveRating(Rating rating){
        ratingRepository.save(rating);
    }

    public void saveRatings(List<Rating> ratings){
        List<Rating> newRatings = new ArrayList<>();

        Rating firstRating = ratings.get(0);
        Optional<User> fetchFirstUser = userRepository.findByName(firstRating.getUser().getName());
        if(fetchFirstUser.isEmpty()){
            userRepository.save(firstRating.getUser());
        }

        for(Rating rating: ratings){
            User user = rating.getUser();
            Optional<User> fetchUser = userRepository.findByName(user.getName());
            if(fetchUser.isPresent()){
                rating.setUser(fetchUser.get());
            } else{
                userRepository.save(user);
                rating.setUser(user);
            }
            newRatings.add(rating);
        }

        updateService(newRatings);
        ratingRepository.saveAll(newRatings);
    }

    private void updateService(List<Rating> ratings){
        if(ratings == null){
            throw new NotFoundException();
        }
        Long questionId = ratings.get(0).getQuestion().getId();
        Long serviceId = serviceRepository.findByQuestionId(questionId);
        Optional<at.tuwien.servcomp.model.Service> serviceOpt = serviceRepository.findById(serviceId);
        if(serviceOpt.isEmpty()){
            throw new NotFoundException();
        }
        at.tuwien.servcomp.model.Service service = serviceOpt.get();

        //Update service scores
        Long reliabilityCount = 0L, assuranceCount = 0L, tangiblesCount = 0L, empathyCount = 0L, responsivenessCount = 0L;
        float reliabilityScore, assuranceScore, tangiblesScore, empathyScore, responsivenessScore;
        for(Rating rating: ratings){
            reliabilityCount += ratingRepository.countByQuestion_DimensionAndQuestion_Id("reliability", rating.getQuestion().getId());
            assuranceCount += ratingRepository.countByQuestion_DimensionAndQuestion_Id("assurance", rating.getQuestion().getId());
            tangiblesCount += ratingRepository.countByQuestion_DimensionAndQuestion_Id("tangibles", rating.getQuestion().getId());
            empathyCount += ratingRepository.countByQuestion_DimensionAndQuestion_Id("empathy", rating.getQuestion().getId());
            responsivenessCount += ratingRepository.countByQuestion_DimensionAndQuestion_Id("responsiveness", rating.getQuestion().getId());
        }

        reliabilityScore = service.getReliabilityScore() * reliabilityCount;
        assuranceScore = service.getAssuranceScore() * assuranceCount;
        tangiblesScore = service.getTangiblesScore() * tangiblesCount;
        empathyScore = service.getEmpathyScore() * empathyCount;
        responsivenessScore = service.getResponsivenessScore() * responsivenessCount;
        for(Rating rating: ratings){
            switch(rating.getQuestion().getDimension()){
                case "reliability":
                    reliabilityScore += rating.getScore();
                    reliabilityCount++;
                    break;
                case "assurance":
                    assuranceScore += rating.getScore();
                    assuranceCount++;
                    break;
                case "tangibles":
                    tangiblesScore += rating.getScore();
                    tangiblesCount++;
                    break;
                case "empathy":
                    empathyScore += rating.getScore();
                    empathyCount++;
                    break;
                case "responsiveness":
                    responsivenessScore += rating.getScore();
                    responsivenessCount++;
                    break;
            }
        }

        service.setReliabilityScore(reliabilityScore/reliabilityCount);
        service.setAssuranceScore(assuranceScore/assuranceCount);
        service.setTangiblesScore(tangiblesScore/tangiblesCount);
        service.setEmpathyScore(empathyScore/empathyCount);
        service.setResponsivenessScore(responsivenessScore/responsivenessCount);
        service.setOverallScore((service.getReliabilityScore() + service.getAssuranceScore()
                + service.getTangiblesScore() + service.getEmpathyScore()
                + service.getResponsivenessScore()) / 5.0f);
        serviceRepository.save(service);
    }
}
