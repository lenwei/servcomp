package at.tuwien.servcomp.service;

import at.tuwien.servcomp.exception.NotFoundException;
import at.tuwien.servcomp.model.Provider;
import at.tuwien.servcomp.model.Question;
import at.tuwien.servcomp.model.Service;
import at.tuwien.servcomp.persistence.ProviderRepository;
import at.tuwien.servcomp.persistence.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceService {

    private ServiceRepository serviceRepository;
    private ProviderRepository providerRepository;

    @Autowired
    public ServiceService(ServiceRepository serviceRepository, ProviderRepository providerRepository){
        this.serviceRepository = serviceRepository;
        this.providerRepository = providerRepository;
    }

    public Service getOneById(Long id){
        return serviceRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<Service> getAll(){
        return serviceRepository.findAll();
    }

    public void saveService(Service service){
        Optional<Provider> provider = providerRepository.findByName(service.getProvider().getName());
        System.out.println(service.getProvider().getName());
        if(provider.isPresent()){
            service.setProvider(provider.get());
        }

        serviceRepository.save(service);
    }

    public List<Question> getQuestionsToService(Long serviceId){
        Optional<Service> service = serviceRepository.findById(serviceId);
        if(service.isEmpty()){
            throw new NotFoundException();
        }
        return service.get().getQuestions();
    }
}
