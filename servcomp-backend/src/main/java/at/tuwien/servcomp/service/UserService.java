package at.tuwien.servcomp.service;

import at.tuwien.servcomp.exception.NotFoundException;
import at.tuwien.servcomp.model.User;
import at.tuwien.servcomp.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User getOneById(Long id){
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public User getOneByName(String name){
        Optional<User> user = userRepository.findByName(name);
        if(user.isPresent()){
            return user.get();
        }
        return null;
    }

    public void saveUser(User user){
        userRepository.save(user);
    }
}
