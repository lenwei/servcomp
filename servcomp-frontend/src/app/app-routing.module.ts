import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListViewComponent } from './components/list-view/list-view.component';
import { OverviewComponent } from './components/overview/overview.component';
import { RatingComponent } from './components/rating/rating.component';
import { CompareComponent } from './components/compare/compare.component';
import { CreateServiceComponent } from './components/create-service/create-service.component';

const routes: Routes = [
  {path: 'main', component: ListViewComponent},
  {path: 'service/add', component: CreateServiceComponent},
  {path: 'service/:id', component: OverviewComponent},
  {path: 'service/rate/:id', component: RatingComponent},
  {path: 'service/:id/compare', component: ListViewComponent},
  {path: 'service/:id/compare/:id2', component: CompareComponent},
  { path: '',   redirectTo: '/main', pathMatch: 'full'}, //redirect
  { path: '**', redirectTo: '/main'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
