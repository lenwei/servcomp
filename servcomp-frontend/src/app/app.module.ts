import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {ChartModule} from 'primeng/chart';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { ListViewComponent } from './components/list-view/list-view.component';
import { CreateServiceComponent } from './components/create-service/create-service.component';
import { OverviewComponent } from './components/overview/overview.component';
import { RatingComponent } from './components/rating/rating.component';
import { CompareComponent } from './components/compare/compare.component';
import { HttpClientModule } from '@angular/common/http';
import { AddQuestionComponent } from './components/create-service/add-question/add-question.component';

@NgModule({
  declarations: [
    AppComponent,
    ListViewComponent,
    CreateServiceComponent,
    OverviewComponent,
    RatingComponent,
    CompareComponent,
    AddQuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ChartModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
