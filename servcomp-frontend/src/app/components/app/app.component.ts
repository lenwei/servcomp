import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  })
  showModal = false;

  constructor(private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    
  }

  login(): void{
    let username = this.loginForm.controls['username'].value;
    let password = this.loginForm.controls['password'].value;
    this.userService.loginServiceManager(username, password);
  }

  checkLogIn(): void{
    let loggedIn = this.userService.getLoggedIn();
    if (loggedIn){
      this.toastr.clear();
      this.infoToast('Successfully logged in!');  
      this.showModal = false;
    }else{
      this.toastr.clear();
      this.errorToast('Wrong credentials!');
    }
  }

  infoToast(message: string): void{
    this.toastr.info(message, '', {
      timeOut: 10000,
      closeButton: true
    });
  }

  errorToast(message: string): void{
    this.toastr.error(message, '', {
      timeOut: 10000,
      closeButton: true
    });
  }

}
