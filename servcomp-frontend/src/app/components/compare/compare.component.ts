import { Component, OnInit } from '@angular/core';
import { ServiceDto } from 'src/app/model/service';
import { ServiceService } from 'src/app/services/service.service';
import { ActivatedRoute } from '@angular/router';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { ExcelService } from 'src/app/services/excel.service';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {

  service1: ServiceDto;
  service2: ServiceDto;
  serviceId1: number;
  serviceId2: number;
  data: any;
  options: any;

  constructor(private serviceService: ServiceService, private route: ActivatedRoute,
    private excelService: ExcelService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serviceId1 = +params['id'];
      this.serviceId2 = +params['id2'];
    });

    this.serviceService.getService(this.serviceId1).subscribe(serv => {
      this.service1 = serv;
      
      this.serviceService.getService(this.serviceId2).subscribe(serv => {
        this.service2 = serv;
        this.initData();
      });
    });

    this.options = {
      title: {
          display: false
      },
      plugins: {
        
      },  
      scales: {
          yAxes: [{
            display: true,
            ticks: {
              suggestedMin: -6,
              suggestedMax: 6,
              stepSize: 1,
              fontSize: 16
            },
            gridLines: {
              color: "rgba(20,20,20,0.1)"
            }
          }],
          xAxes: [{
            display: true,
            ticks: {
              fontSize: 16,
              fontColor: 'black'
            },
            gridLines: {
              color: "rgba(0,0,0,0)"
            }
          }]
      },
      animation: {
        duration: 10,
        easing: "linear"
      },
      tooltips: {
          enabled: false
      },
      legend: {
          display: false
      }
  };

  }

  initData(): void{
    this.data = {
      labels: ['Reliability', 'Assurance', 'Tangibles', 'Empathy', 'Responsiveness'],
      datasets: [
        {
          backgroundColor: [
            this.getColor1(this.service1.reliabilityScore),
            this.getColor1(this.service1.assuranceScore),
            this.getColor1(this.service1.tangiblesScore),
            this.getColor1(this.service1.empathyScore),
            this.getColor1(this.service1.responsivenessScore),
          ],
          barPercentage: 0.9,
          barThickness: 'flex',
          data: [
            this.service1.reliabilityScore,
            this.service1.assuranceScore,
            this.service1.tangiblesScore,
            this.service1.empathyScore,
            this.service1.responsivenessScore
          ],
          datalabels: {
            color: 'black',
            labels: {
              title: {
                  font: {
                      weight: 'bold',
                      size: 16
                  }
              },
            },
            anchor: [
              this.getAlign(this.service1.reliabilityScore),
              this.getAlign(this.service1.assuranceScore),
              this.getAlign(this.service1.tangiblesScore),
              this.getAlign(this.service1.empathyScore),
              this.getAlign(this.service1.responsivenessScore)
            ],
            align: [
              this.getAlign(this.service1.reliabilityScore),
              this.getAlign(this.service1.assuranceScore),
              this.getAlign(this.service1.tangiblesScore),
              this.getAlign(this.service1.empathyScore),
              this.getAlign(this.service1.responsivenessScore)
            ],
            formatter: function(value, context) {
              return formatNumber(value, 'en-US', '1.0-2');
            }
          }
        },
        {
          backgroundColor: [
            this.getColor2(this.service2.reliabilityScore),
            this.getColor2(this.service2.assuranceScore),
            this.getColor2(this.service2.tangiblesScore),
            this.getColor2(this.service2.empathyScore),
            this.getColor2(this.service2.responsivenessScore),
          ],
          barPercentage: 0.9,
          data: [
            this.service2.reliabilityScore,
            this.service2.assuranceScore,
            this.service2.tangiblesScore,
            this.service2.empathyScore,
            this.service2.responsivenessScore
          ],
          datalabels: {
            color: 'black',
            labels: {
              title: {
                  font: {
                      weight: 'bold',
                      size: 16
                  }
              },
            },
            anchor: [
              this.getAlign(this.service2.reliabilityScore),
              this.getAlign(this.service2.assuranceScore),
              this.getAlign(this.service2.tangiblesScore),
              this.getAlign(this.service2.empathyScore),
              this.getAlign(this.service2.responsivenessScore)
            ],
            align: [
              this.getAlign(this.service2.reliabilityScore),
              this.getAlign(this.service2.assuranceScore),
              this.getAlign(this.service2.tangiblesScore),
              this.getAlign(this.service2.empathyScore),
              this.getAlign(this.service2.responsivenessScore)
            ],
            formatter: function(value, context) {
              return formatNumber(value, 'en-US', '1.0-2');
            }
          }
        }
      ],
      plugin: [ChartDataLabels]    
    }
  }

  getColor1(value: number): string{
    if(value >= 0){
      return '#22bb22';
    } else{
      return '#cc0000';
    }
  }

  getColor2(value: number): string{
    if(value >= 0){
      return 'darkgreen';
    } else{
      return '#800000';
    }
  }

  getAlign(value: number): string{
    if(value >= 0){
      return 'end';
    } else{
      return 'start';
    }
  }

  exportAsXLSX(): void{
    let data: any = [{
      "Name of Service": this.service1.name,
      Reliability: this.service1.reliabilityScore,
      Assurance: this.service1.assuranceScore,
      Tangibles: this.service1.tangiblesScore,
      Empathy: this.service1.empathyScore,
      Responsiveness: this.service1.responsivenessScore,
      OverallScore: this.service1.overallScore
      },
      {
      "Name of Service": this.service2.name,
      Reliability: this.service2.reliabilityScore,
      Assurance: this.service2.assuranceScore,
      Tangibles: this.service2.tangiblesScore,
      Empathy: this.service2.empathyScore,
      Responsiveness: this.service2.responsivenessScore,
      OverallScore: this.service2.overallScore
      }
    ];
    this.excelService.exportAsExcelFile(data, "compare_" + this.service1.name.replace(' ', '_').toLowerCase().slice(0,20) + "_" +
     this.service2.name.replace(' ', '_').toLowerCase().slice(0,20));
  }

}
