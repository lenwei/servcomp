import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray } from '@angular/forms';
import { QuestionDto } from 'src/app/model/question';
import { ServiceDto } from 'src/app/model/service';
import { ServiceService } from 'src/app/services/service.service';
import { ProviderDto } from 'src/app/model/provider';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.scss']
})
export class CreateServiceComponent implements OnInit {
  
  questionArray: FormGroup = new FormGroup({
    serviceName: new FormControl(''),
    providerName: new FormControl(''),
    reliability: new FormArray([]),
    assurance: new FormArray([]),
    tangibles: new FormArray([]),
    empathy: new FormArray([]),
    responsiveness: new FormArray([]),
  })
  dimensions: string[] = ["reliability", "assurance", "tangibles", "empathy", "responsiveness"];

  constructor(private serviceService: ServiceService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(): void{
    if(this.validateInput() == false){
      return;
    }

    let service: ServiceDto = new ServiceDto(0,null,
      new ProviderDto(0, this.questionArray.controls['providerName'].value), this.questionArray.controls['serviceName'].value, 0,0,
      0,0,0,0);
    let questions: QuestionDto[] = [];
    for(let i = 2; i <= 6; i++){
      let array: FormArray = <FormArray>this.questionArray.controls[this.dimensions[i-2]];
      for(let group of array.controls){
        let question: QuestionDto = new QuestionDto(1, this.dimensions[i-2] ,group.value.expectation,
          group.value.perception);
        questions.push(question);
      }
    }
    service.questions = questions;
    this.serviceService.addService(service).subscribe();
    this.showToast();
    this.router.navigate(['/main']);
  }

  validateInput(): boolean{
    if(this.questionArray.controls['serviceName'].value === '' ){
      this.errorToast('Please enter a service name!');
      return false;
    }

    if(this.questionArray.controls['providerName'].value === '' ){
      this.errorToast('Please enter a provider name!');
      return false;
    }

    if((<FormArray>this.questionArray.controls['reliability']).length === 0
    || (<FormArray>this.questionArray.controls['assurance']).length === 0
    || (<FormArray>this.questionArray.controls['tangibles']).length === 0
    || (<FormArray>this.questionArray.controls['empathy']).length === 0
    || (<FormArray>this.questionArray.controls['responsiveness']).length === 0){
      this.errorToast('Each dimensions must have at least one question!');
      return false;
    }
  }

  showToast(): void{
    this.toastr.info('Service added!', '', {
      timeOut: 10000,
      closeButton: true
    });
  }

  errorToast(message: string): void{
    this.toastr.error(message, '', {
      timeOut: 10000,
      closeButton: true
    });
  }

  addQuestion(dimension: string): void{
    let array: FormArray = <FormArray>this.questionArray.controls[dimension];
    array.push(
      new FormGroup({
        expectation: new FormControl(''),
        perception: new FormControl('')
      })
    );
  }

  trackByFn(index, item) {
    return index;
  }
}
