import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { ServiceDto } from 'src/app/model/service';
import { ActivatedRoute, UrlSegment, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {

  serviceId: number;
  select: boolean;
  link: any;
  services$: Observable<ServiceDto[]>;
  filteredServices$: Observable<ServiceDto[]>;
  filter: FormControl;
  filter$: Observable<string>;

  constructor(private serviceService: ServiceService, private route: ActivatedRoute,
    public router: Router, private userService: UserService) {
      this.services$ = this.serviceService.getServices();
      this.filter = new FormControl('');
      this.filter$ = this.filter.valueChanges.pipe(startWith(''));

      //Dynamic filtering: https://blog.angulartraining.com/dynamic-filtering-with-rxjs-and-angular-forms-a-tutorial-6daa3c44076a
      this.filteredServices$ = combineLatest(this.services$, this.filter$).pipe(
        map(([services, filterString]) => 
          services.filter(service => 
            service.name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1))
      );    
     }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serviceId = +params['id'];
    });

    let url: UrlSegment[] = this.route.snapshot.url;
    if(url[2] != null && url[2].toString() === 'compare'){
        this.select = true;
    }
  }

  onClick(service1: number, service2: number): void{
    if(this.select){
      this.link = '/service/'+ service1 +'/compare/'+ service2;
    }else{
      this.link = '/service/'+ service2;
    }

    this.router.navigate([this.link]);
  }

}
