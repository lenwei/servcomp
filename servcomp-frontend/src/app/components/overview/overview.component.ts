import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { ServiceDto } from 'src/app/model/service';
import { ActivatedRoute } from '@angular/router';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { ExcelService } from 'src/app/services/excel.service';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  service: ServiceDto;
  serviceId: number;
  data: any;
  options: any;
  constructor(private serviceService: ServiceService, private route: ActivatedRoute,
    private excelService: ExcelService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serviceId = +params['id'];
    });

    this.serviceService.getService(this.serviceId).subscribe(serv => {
      this.service = serv;

      this.initData();
    });

    this.options = {
      title: {
          display: false
      },
      plugins: {
        
      },  
      scales: {
          yAxes: [{
            display: true,
            ticks: {
              suggestedMin: -6,
              suggestedMax: 6,
              stepSize: 1,
              fontSize: 16
            },
            gridLines: {
              color: "rgba(20,20,20,0.1)"
            }
          }],
          xAxes: [{
            display: true,
            ticks: {
              fontSize: 16,
              fontColor: 'black'
            },
            gridLines: {
              color: "rgba(0,0,0,0)"
            }
          }]
      },
      animation: {
        duration: 10,
        easing: "linear"
      },
      tooltips: {
          enabled: false
      },
      legend: {
          display: false
      }
  };
  }

  initData(): void{
    this.data = {
      labels: ['Reliability', 'Assurance', 'Tangibles', 'Empathy', 'Responsiveness'],
      datasets: [
          {
              backgroundColor: [
                this.getColor(this.service.reliabilityScore),
                this.getColor(this.service.assuranceScore),
                this.getColor(this.service.tangiblesScore),
                this.getColor(this.service.empathyScore),
                this.getColor(this.service.responsivenessScore),
              ],
              barPercentage: 0.6,
              data: [
                this.service.reliabilityScore,
                this.service.assuranceScore,
                this.service.tangiblesScore,
                this.service.empathyScore,
                this.service.responsivenessScore
              ],
              datalabels: {
                color: 'black',
                labels: {
                  title: {
                      font: {
                          weight: 'bold',
                          size: 16
                      }
                  },
                },
                anchor: [
                  this.getAlign(this.service.reliabilityScore),
                  this.getAlign(this.service.assuranceScore),
                  this.getAlign(this.service.tangiblesScore),
                  this.getAlign(this.service.empathyScore),
                  this.getAlign(this.service.responsivenessScore)
                ],
                align: [
                  this.getAlign(this.service.reliabilityScore),
                  this.getAlign(this.service.assuranceScore),
                  this.getAlign(this.service.tangiblesScore),
                  this.getAlign(this.service.empathyScore),
                  this.getAlign(this.service.responsivenessScore)
                ],
                formatter: function(value, context) {
                  return formatNumber(value, 'en-US', '1.0-2');
                }
              }
          }
        ],
        plugin:[ChartDataLabels]
    }
  }

  getColor(value: number): string{
    if(value >= 0){
      return '#22bb22';
    } else{
      return '#cc0000';
    }
  }

  getAlign(value: number): string{
    if(value >= 5){
      return 'start';
    }
    if(value <= -5){
      return 'end';
    }

    if(value >= 0){
      return 'end';
    } else{
      return 'start';
    }
  }

  exportAsXLSX(): void {
    let data: any = [{
      "Name of Service": this.service.name,
      Reliability: this.service.reliabilityScore,
      Assurance: this.service.assuranceScore,
      Tangibles: this.service.tangiblesScore,
      Empathy: this.service.empathyScore,
      Responsiveness: this.service.responsivenessScore,
      OverallScore: this.service.overallScore
      }
    ];
    this.excelService.exportAsExcelFile(data, this.service.name.replace(' ', '_').toLowerCase());
  }
}
