import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ServiceDto } from 'src/app/model/service';
import { ServiceService } from 'src/app/services/service.service';
import { QuestionDto } from 'src/app/model/question';
import { FormControl } from '@angular/forms';
import { RatingDto } from 'src/app/model/rating';
import { UserDto } from 'src/app/model/user';
import { RatingService } from 'src/app/services/rating.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  serviceId: number;
  service: ServiceDto;
  questions: QuestionDto[];
  ratings: RatingDto[];
  warning: boolean = false;
  userName: FormControl = new FormControl('');
  dimensions = [{
    "dimension": "reliability",
    "questions": []
  },
  {
    "dimension": "assurance",
    "questions": []
  },
  {
    "dimension": "tangibles",
    "questions": []
  },
  {
    "dimension": "empathy",
    "questions": []
  },
  {
    "dimension": "responsiveness",
    "questions": []
  }];
  expectation00: string = '';

  constructor(private route: ActivatedRoute, private serviceService: ServiceService,
    private location: Location, private ratingService: RatingService,
    private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.serviceId = +params['id'];
    });

    this.serviceService.getService(this.serviceId).subscribe(
      serv => this.service = serv
    );  

    this.serviceService.getQuestionsToService(this.serviceId).subscribe(
      questions => {
        this.questions = questions;
        this.questionSetup(this.questions);
      }
    );
  }

  questionSetup(questions: QuestionDto[]): void{
    for(let question of questions){
      for(let dimension of this.dimensions){
        if(question.dimension === dimension.dimension){
          dimension.questions.push(question);
        }
      }
    }
  }

  onSubmit(): boolean{
    if(this.userName.value === ''){
      this.errorToast('Please enter a username!');
      return false;
    }

    let ratings: RatingDto[] = [];
    let flag: boolean = false;
    
    for(let dimension of this.dimensions){
      dimension.questions.forEach((value, index) => {
        let expectation: number, perception: number;
        let exp_boxes = <NodeListOf<HTMLInputElement>>document.getElementsByName("expectation_"+dimension.dimension+index);
        let val1: boolean = false;
        for(var i = 0; i < exp_boxes.length; i++){
          if(exp_boxes[i].checked){
            expectation = +exp_boxes[i].value;
            val1 = true;
          }
        }

        let perc_boxes = <NodeListOf<HTMLInputElement>>document.getElementsByName("perception_"+dimension.dimension+index);
        let val2: boolean = false;
        for(var i = 0; i < perc_boxes.length; i++){
          if(perc_boxes[i].checked){
            perception = +perc_boxes[i].value;
            val2 = true;
          }
        }
        if(val1==false || val2==false){
          flag = true;
        }

        let user: UserDto;
        let rating: RatingDto;
        user = new UserDto(0, this.userName.value);
        rating = new RatingDto(0, value, user, perception - expectation);
        
        ratings.push(rating);
      });

      if(flag){
        this.errorToast('Please answer all questions!');
        return false;
      }
    }
    
    this.ratingService.addRatings(ratings).subscribe();

    this.showToast();
    this.router.navigate(['/service/'+this.serviceId]);
    return true;
  }

  showToast(): void{
    this.toastr.info('Service rated!', '', {
      timeOut: 10000,
      closeButton: true
    });
  }

  errorToast(message: string): void{
    this.toastr.error(message, '', {
      timeOut: 10000,
      closeButton: true
    });
  }

  goBack(): void {
    this.location.back();
  }
}
