
export class QuestionDto{
    constructor(
        public id: number,
        public dimension: string,
        public expectation: string,
        public perception: string){
    }
}