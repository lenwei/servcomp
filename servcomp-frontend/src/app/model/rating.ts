import { QuestionDto } from './question';
import { UserDto } from './user';

export class RatingDto{
    constructor(
        public id: number,
        public question: QuestionDto, 
        public user: UserDto,
        public score: number){
    }
}