import { QuestionDto } from './question';
import { ProviderDto } from './provider';

export class ServiceDto{
    constructor(
        public id: number,
        public questions: QuestionDto[],
        public provider: ProviderDto,
        public name: string,
        public overallScore: number,
        public reliabilityScore: number,
        public assuranceScore: number,
        public tangiblesScore: number,
        public empathyScore: number,
        public responsivenessScore: number){
    }
}