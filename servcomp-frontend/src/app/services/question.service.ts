import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QuestionDto } from '../model/question';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  baseUri = 'http://localhost:8080/question';

  constructor(private http: HttpClient) { }

  getQuestions(serviceId: number): Observable<QuestionDto[]>{
    console.log("Getting all questions for service " + serviceId);
    return this.http.get<QuestionDto[]>(this.baseUri + '/by-service/' + serviceId);
  }
}
