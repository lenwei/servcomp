import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RatingDto } from '../model/rating';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  baseUri = 'http://localhost:8080/rating';

  constructor(private http: HttpClient) { }

  addRatings(ratings: RatingDto[]): Observable<RatingDto[]>{
    console.log("Posting ratings");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<RatingDto[]>(this.baseUri+"/ratings", ratings, httpOptions);
  }
}
