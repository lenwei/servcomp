import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ServiceDto } from '../model/service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { QuestionDto } from '../model/question';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  baseUri = 'http://localhost:8080/service';

  constructor(private http: HttpClient) { }

  getServices(): Observable<ServiceDto[]>{
    return this.http.get<ServiceDto[]>(this.baseUri + '/services');
  }

  getService(id: number): Observable<ServiceDto>{
    return this.http.get<ServiceDto>(this.baseUri + '/' + id);
  }

  addService(service: ServiceDto): Observable<ServiceDto>{
    console.log("Posting new service");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<ServiceDto>(this.baseUri, service, httpOptions);
  }

  getQuestionsToService(serviceId: number): Observable<QuestionDto[]>{
    console.log("Getting all questions for service " + serviceId);
    return this.http.get<QuestionDto[]>(this.baseUri + '/by-service/' + serviceId);
  }

}
