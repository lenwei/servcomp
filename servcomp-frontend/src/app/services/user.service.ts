import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDto } from '../model/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUri = 'http://localhost:8080/user';
  loggedIn: boolean = false;

  constructor(private http: HttpClient) { }

  getUserByName(userName: string): Observable<UserDto>{
    return this.http.get<UserDto>(this.baseUri + '/by-name/' + userName);
  }

  //Hard-coded credentials for initial version of the system. 
  //Should be changed to a decent login system
  loginServiceManager(userName: string, password: string): void {
    if (userName === "manager" && password === "manager"){
      this.loggedIn = true;
    }else{
      this.loggedIn = false;
    }
  }

  getLoggedIn(): boolean{
    return this.loggedIn;
  }

}
